﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace test_Parser
{ 
public struct ItemFromLog
{
  public int serial_number;
  public string user;
  public string organization;
  public string ip_address;
  public string unique_session_ID;
  public DateTime login_date_time;
  public DateTime logout_date_time;
  public int session_status;

  public ItemFromLog(int serial_number_, string user_, string organization_, string ip_address_, string unique_session_ID_,
    DateTime login_date_time_, DateTime logout_date_time_, int session_status_)
  {
    this.serial_number = serial_number_;
    this.user = user_;
    this.organization = organization_;
    this.ip_address = ip_address_;
    this.unique_session_ID = unique_session_ID_;
    this.login_date_time = login_date_time_;
    this.logout_date_time = logout_date_time_;
    this.session_status = session_status_;
  }
}

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
  {

  // Функция проверки корректности строки. Возвращает true - строка верная, false - некорректная строка
  public bool CheckingTheStringForCorrectness(string line)
    {
      // проверка на количество элементов в строке.
      string[] subs = line.Split(';');
      if (subs.Length != 9)
      {
        return false;
      }

      // проверка ip адрес
      Regex ip_regex = new Regex(@"^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]?)$");
      if (!ip_regex.IsMatch(subs[3]))
      {
        return false;
      }

      // проверка дат
      Regex datetime_regex = new Regex(@"\d{1,2}.\d{1,2}.\d{4}\s\d{1,2}:\d{1,2}:\d{1,2}");
      if (!datetime_regex.IsMatch(subs[5]) || !datetime_regex.IsMatch(subs[6]))
      {
        return false;
      }

      return true;
    }

  public Tuple<int,ItemFromLog> GenereateItemFromLogFromLine(string line)
  {
    string[] subs = line.Split(';');
    DateTime login_date_time = Convert.ToDateTime(subs[5]);
    DateTime logout_date_time = Convert.ToDateTime(subs[6]);

      ItemFromLog result = new ItemFromLog(Int32.Parse(subs[0]), subs[1], subs[2], subs[3], subs[4], login_date_time, logout_date_time, Int32.Parse(subs[7]));
    return Tuple.Create(result.serial_number, result);
  }

    // Функция для парсинга txt файла и создания коллекции данных на данных из txt файла.
    public Dictionary<int, ItemFromLog> ParseTXTFile()
    {
      Dictionary<int, ItemFromLog> result = new Dictionary<int, ItemFromLog>();

      string path = Directory.GetCurrentDirectory() + "\\ERROR.txt";
      string[] lines = System.IO.File.ReadAllLines("LOG.txt");
      foreach (string line in lines)
      {
        if (!CheckingTheStringForCorrectness(line)) // Если строка не прошла проверку
        {
          if (!File.Exists(path))
          {
            File.WriteAllText(path, line + "\n", Encoding.UTF8);
          }
          File.AppendAllText(path, line + "\n", Encoding.UTF8);
        }
        else // Если строка прошла проверку
        {
          Tuple<int, ItemFromLog> generatedItem = GenereateItemFromLogFromLine(line);
          result.Add(generatedItem.Item1, generatedItem.Item2);
        }
      }
      if (File.Exists(path))
      {
        File.AppendAllText(path, "------------------------------------\n", Encoding.UTF8);
      }

      return result;
    }
    public MainWindow()
    {
      Dictionary<int, ItemFromLog> generatedDict = ParseTXTFile();
      Console.WriteLine(generatedDict.Count.ToString());
      InitializeComponent();
    }
  }
}
